@echo off
title System Starter

set node-ver=v14.17.1

echo Bootstrapping...
goto download

:download
if exist "node.zip" goto unzip
powershell -Command "Invoke-WebRequest https://nodejs.org/dist/%node-ver%/node-%node-ver%-win-x64.zip -OutFile node.zip"

:unzip
if exist "node\" goto get-git
powershell -Command Expand-Archive node.zip -DestinationPath node\

:get-git
if exist "git\" goto get-core
if exist "git-packed.exe" goto unzip-git
powershell -Command "Invoke-WebRequest https://github.com/git-for-windows/git/releases/download/v2.32.0.windows.1/PortableGit-2.32.0-64-bit.7z.exe -OutFile git-packed.exe"

:unzip-git
git-packed.exe -o"git\" -y

:get-core
if exist "core\" goto start-core
call "git\bin\git.exe" "clone" "https://gitlab.com/system_starter/core.git"

:start-core
call "node\node-%node-ver%-win-x64\npm.cmd" "install"
call "node\node-%node-ver%-win-x64\node.exe" "core\index.js"
